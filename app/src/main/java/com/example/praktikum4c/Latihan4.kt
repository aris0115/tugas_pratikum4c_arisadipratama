package com.example.praktikum4c

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class Latihan4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_latihan4)

        val bt_proses = findViewById<Button>(R.id.bt_proses)
        val et_nim = findViewById<EditText>(R.id.et_nim)
        val et_nama = findViewById<EditText>(R.id.et_nama)
        val et_prodi = findViewById<EditText>(R.id.et_prodi)

        bt_proses.setOnClickListener{
            //val strValue: String = et_nama.getText().toString()
            val hasil = "Halo , perkenalkan saya " + et_nama.getText().toString() + " dengan NIM " + et_nim.getText().toString() + " pada Program Studi " + et_prodi.getText().toString()
            Toast.makeText(applicationContext,hasil, Toast.LENGTH_SHORT).show()
        }
    }
}