package com.example.praktikum4c

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class Tugaspbo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tugaspbo)

        val bt_tugas = findViewById<Button>(R.id.bt_tugas)
        val et_nama_tugas= findViewById<EditText>(R.id.et_nama_tugas)

        bt_tugas.setOnClickListener{
                val angka = Integer.parseInt(et_nama_tugas.getText().toString())
                if (angka%2== 0) {
                    Toast.makeText(getApplication(),"Ini Bilangan Genap ",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplication(),"Ini Bilangan Ganjil ",Toast.LENGTH_SHORT).show();
                }
        }
    }
}