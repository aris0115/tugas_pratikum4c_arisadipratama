package com.example.praktikum4c

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class Latihan2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_latihan2)

        val bt_proses = findViewById<Button>(R.id.bt_prs) as Button
        val et_latihan2_nama = findViewById<EditText>(R.id.et_latihan2_nama) as EditText
        val et_latihan2_hasil = findViewById<EditText>(R.id.et_latihan2_hasil) as EditText

        bt_proses.setOnClickListener{
            et_latihan2_hasil.text = et_latihan2_nama.text
        }
    }
}